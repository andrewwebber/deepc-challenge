module deepc

go 1.13

replace github.com/grailbio/go-dicom => ./thirdparty/github.com/grailbio/go-dicom

require (
	github.com/gorilla/mux v1.7.3
	github.com/grailbio/go-dicom v0.0.0
	github.com/grailbio/go-netdicom v0.0.0-20190401004905-5b21cc7896c8
	github.com/pkg/errors v0.8.1
	github.com/suyashkumar/dicom v0.4.0
)
