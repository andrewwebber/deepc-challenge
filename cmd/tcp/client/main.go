package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/grailbio/go-dicom"
	"github.com/grailbio/go-netdicom"
	"github.com/grailbio/go-netdicom/sopclass"
)

func main() {
	var (
		uploadfile    = flag.String("uploadfile", "./testdata/IM-0001-0003.dcm", "File to upload to DICOM server")
		serverAddress = flag.String("server-address", "35.234.129.100", "DICOM Server ip address")
		serverPort    = flag.String("server-port", "3000", "DICOM server port")
	)

	user, err := netdicom.NewServiceUser(netdicom.ServiceUserParams{SOPClasses: sopclass.StorageClasses})
	if err != nil {
		log.Fatal(err)
	}

	user.Connect(fmt.Sprintf("%s:%s", *serverAddress, *serverPort))

	ds, err := dicom.ReadDataSetFromFile(*uploadfile, dicom.ReadOptions{})
	if err != nil {
		log.Fatal(err)
	}

	err = user.CStore(ds)
	if err != nil {
		log.Fatal(err)
	}

	user.Release()
}
