package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"
	"github.com/grailbio/go-dicom"
	"github.com/grailbio/go-dicom/dicomio"
	"github.com/grailbio/go-dicom/dicomtag"
	"github.com/grailbio/go-dicom/dicomuid"
	"github.com/grailbio/go-netdicom"
	"github.com/grailbio/go-netdicom/dimse"
)

func main() {
	var (
		storagePath                = flag.String("storage-path", "/tmp", "storage path for all incoming C-Store commands")
		dicomServerPort            = flag.Int("dicom-server-port", 3000, "DICOM server listening port")
		dicomServerHealthCheckPort = flag.Int("dicom-server-healthcheck-port", 3001, "DICOM server health check listening port")
		webServerPort              = flag.Int("web-server-port", 8080, "Web server listening port")
	)

	flag.Parse()

	log.Printf("Storage path for DICOM files - %s", *storagePath)

	s, err := netdicom.NewServiceProvider(netdicom.ServiceProviderParams{
		AETitle: "deepc-challenge",
		CEcho:   echoCallback(),
		CStore:  storeCallback(*storagePath),
	}, fmt.Sprintf(":%d", *dicomServerPort))

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Starting server on port %d", *dicomServerPort)

	if err = runHealthCheckListener(*dicomServerHealthCheckPort); err != nil {
		log.Fatal(err)
	}

	router := mux.NewRouter()
	router.HandleFunc("/", filesHandler(*storagePath))
	router.HandleFunc("/metadata/{file}", metadataHandler(*storagePath))
	router.HandleFunc("/sop/{file}", fileHandler(*storagePath))
	router.HandleFunc("/health", webHealthHandler())

	http.Handle("/", router)
	go http.ListenAndServe(fmt.Sprintf(":%d", *webServerPort), nil)

	s.Run()
}
func dicomFilePath(storagePath string, file string) string {
	return filepath.Join(storagePath, fmt.Sprintf("%s.dcm", file))
}

func webHealthHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(http.StatusOK)
	}
}

func imageForDataSet(ds *dicom.DataSet) []byte {
	for _, elem := range ds.Elements {
		if elem.Tag == dicomtag.PixelData {
			data := elem.Value[0].(dicom.PixelDataInfo)
			for _, frame := range data.Frames {
				return frame
			}
		}
	}

	return nil
}

func fileHandler(storagePath string) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		file, ok := vars["file"]
		if !ok {
			http.Error(w, "invalid file", http.StatusBadRequest)
			return
		}

		files, err := sopFiles(storagePath)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var fileExits bool
		for _, f := range files {
			if f == file {
				fileExits = true
				break
			}
		}

		if !fileExits {
			http.Error(w, "", http.StatusNotFound)
			return
		}

		filePath := dicomFilePath(storagePath, file)
		log.Printf("Serving file - %s", filePath)
		openFile, err := os.Open(filePath)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		defer openFile.Close()

		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.dcm;", file))
		w.WriteHeader(http.StatusOK)
		io.Copy(w, openFile)
		return
	}
}

func metadataHandler(storagePath string) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		file, ok := vars["file"]
		if !ok {
			http.Error(w, "invalid file", http.StatusBadRequest)
			return
		}

		ds, err := dicom.ReadDataSetFromFile(dicomFilePath(storagePath, file), dicom.ReadOptions{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		enc := json.NewEncoder(w)
		if err = enc.Encode(ds.Elements); err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}
func sopFiles(storagePath string) ([]string, error) {
	var files []string

	err := filepath.Walk(storagePath, func(path string, info os.FileInfo, walkErr error) error {
		if walkErr != nil {
			return nil
		}

		if filepath.Ext(info.Name()) == ".dcm" {
			ds, err := dicom.ReadDataSetFromFile(path, dicom.ReadOptions{DropPixelData: true})
			if err != nil {
				log.Println(err)
				return nil
			}

			sopInstanceUIDElement, err := ds.FindElementByTag(dicomtag.SOPInstanceUID)
			files = append(files, sopInstanceUIDElement.MustGetString())
		}

		return nil
	})

	return files, err
}

func filesHandler(storagePath string) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		files, err := sopFiles(storagePath)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		enc := json.NewEncoder(w)
		if err = enc.Encode(files); err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func runHealthCheckListener(port int) error {
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}

	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				log.Printf("Health check connection error - %v", err)
			}

			log.Println("Health check connection received")

			_ = conn.Close()
		}
	}()

	log.Printf("Started health check listener on port %v", port)

	return nil
}

func echoCallback() netdicom.CEchoCallback {
	return func(conn netdicom.ConnectionState) dimse.Status {
		fmt.Println("C-ECHO call")
		return dimse.Success
	}
}

func storeCallback(storagePath string) netdicom.CStoreCallback {
	return func(
		connState netdicom.ConnectionState,
		transferSyntaxUID string,
		sopClassUID string,
		sopInstanceUID string,
		data []byte) dimse.Status {

		log.Printf("Start C-STORE handler, transfersyntax=%s, sopclass=%s, sopinstance=%s",
			dicomuid.UIDString(transferSyntaxUID),
			dicomuid.UIDString(sopClassUID),
			dicomuid.UIDString(sopInstanceUID))

		path := filepath.Join(storagePath, fmt.Sprintf("%s.dcm", sopInstanceUID))
		e := dicomio.NewBytesEncoderWithTransferSyntax(transferSyntaxUID)

		dicom.WriteFileHeader(e,
			[]*dicom.Element{
				dicom.MustNewElement(dicomtag.TransferSyntaxUID, transferSyntaxUID),
				dicom.MustNewElement(dicomtag.MediaStorageSOPClassUID, sopClassUID),
				dicom.MustNewElement(dicomtag.MediaStorageSOPInstanceUID, sopInstanceUID),
			})
		e.WriteBytes(data)
		cstoreData := e.Bytes()

		if err := ioutil.WriteFile(path, cstoreData, 0766); err != nil {
			log.Printf("C-STORE: Error - %v", err)
			return dimse.Status{
				Status:       dimse.StatusInvalidArgumentValue,
				ErrorComment: err.Error(),
			}
		}

		log.Printf("C-STORE: Created %v", path)

		return dimse.Success
	}
}
