package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/grailbio/go-dicom"
	"github.com/grailbio/go-dicom/dicomtag"
	"github.com/grailbio/go-netdicom"
	"github.com/grailbio/go-netdicom/sopclass"
)

func TestMain(m *testing.M) {
	log.SetFlags(log.Llongfile)
	os.Exit(m.Run())
}

func TestWriteRead(t *testing.T) {
	ds, err := dicom.ReadDataSetFromFile("../../../testdata/image-000001.dcm", dicom.ReadOptions{})
	if err != nil {
		t.Fatal(err)
	}

	if err = dicom.WriteDataSetToFile("/tmp/testfile.dcm", ds); err != nil {
		t.Fatal(err)
	}

	ds, err = dicom.ReadDataSetFromFile("/tmp/testfile.dcm", dicom.ReadOptions{})
	if err != nil {
		t.Fatal(err)
	}

	imageBytes := imageForDataSet(ds)
	if imageBytes == nil {
		t.Fatal("expected image bytes")
	}
}

func TestStore(t *testing.T) {
	s, err := netdicom.NewServiceProvider(netdicom.ServiceProviderParams{
		CEcho:  echoCallback(),
		CStore: storeCallback("/tmp"),
	}, ":3000")
	if err != nil {
		panic(err)
	}
	go s.Run()

	user, err := netdicom.NewServiceUser(netdicom.ServiceUserParams{SOPClasses: sopclass.StorageClasses})
	user.Connect("127.0.0.1:3000")

	ds, err := dicom.ReadDataSetFromFile("../../../testdata/IM-0001-0003.dcm", dicom.ReadOptions{})
	if err != nil {
		t.Fatal(err)
	}

	sopInstanceUIDElement, err := ds.FindElementByTag(dicomtag.SOPInstanceUID)
	if err != nil {
		t.Fatal(err)
	}

	err = user.CStore(ds)
	if err != nil {
		t.Fatal(err)
	}

	user.Release()

	rc := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/files", nil)
	handler := filesHandler("/tmp")
	handler(rc, req)
	result := rc.Result()
	defer result.Body.Close()

	if result.StatusCode != http.StatusOK {
		t.Fatal(result.Status)
	}

	dec := json.NewDecoder(result.Body)
	var filesResult []string
	if err = dec.Decode(&filesResult); err != nil {
		t.Fatal(err)
	}

	t.Logf("%+v", filesResult)
	if len(filesResult) == 0 {
		t.Fatalf("expected to file dcm files - %+v", filesResult)
	}

	var foundCStoreFile bool
	for _, file := range filesResult {
		if strings.Contains(file, sopInstanceUIDElement.MustGetString()) {
			foundCStoreFile = true
			break
		}
	}

	if !foundCStoreFile {
		t.Fatalf("expected to find file %s.dcm", sopInstanceUIDElement.MustGetString())
	}
}
