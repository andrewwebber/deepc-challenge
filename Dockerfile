FROM golang:1.13.6-alpine as build-env

WORKDIR /tmp/deepc
ADD . /tmp/deepc

RUN CGO_ENABLED=0 go build -o deepc-challenge-tcp ./cmd/tcp/server
RUN ls -alh && pwd
FROM alpine
COPY --from=build-env /tmp/deepc/deepc-challenge-tcp /
ENTRYPOINT ["/deepc-challenge-tcp"]
CMD ["-help"]

