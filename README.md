## DICOM
Overview - https://book.orthanc-server.com/dicom-guide.html

### Format
- Image + Patient data (name, sex, DOB)

Browser - https://dicom.innolitics.com/ciods/cr-image

### Data organization
```mermaid
classDiagram
      Store "1" --> "*" Patient
      Patient "1" --> "*" Study
      Study "1" --> "*" Series
      Series "1" --> "*" Image

```

### Services
#### Operations
Move Find Store

### Protocols
#### DICOM 

Service Class User (SCU) Client
Service Class Provider (SCP) Server

```mermaid
sequenceDiagram
    SCU Client-->>SCP Server: A-Associate-RQ
    alt is Reject
        SCP Server-->>SCU Client: A-Associate-RJ
    else is Accept
        SCP Server-->>SCU Client: A-Associate-AC
    end
    SCU Client-->SCP Server: P-DATA-TF
    SCU Client-->>SCP Server: A-Release-RQ
    alt is Abort
        SCP Server-->>SCU Client: A-Abort
    else is Release
        SCP Server-->>SCU Client: A-Release-RSP
    end
    SCU Client-->>SCP Server: A-Abort
```

Libraries
- https://github.com/grailbio/go-netdicom
- https://github.com/suyashkumar/dicom

#### JP2000 to JPG
mogrify -format jpg ./image.0.jpg

#### DICOM Web
Web protocols for DICOM

Web Tester - https://github.com/levinalex/orthanctool/tree/master/api

Test Files https://github.com/suyashkumar/dicom/tree/master/examples

### DICOM Viewer
https://ivmartel.github.io/dwv/
https://github.com/ivmartel/dwv-react
weasis

## Emulation
https://github.com/dvtk-org/DVTk
