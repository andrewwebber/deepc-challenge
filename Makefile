all: build test docker-image

build: bin bin/binaries

bin:
	mkdir ./bin

clean:
	rm -rf ./bin

bin/binaries:
	go build -o ./bin -v ./...

test:
	go test -v ./...

docker-image: build
	docker build -t andrewvwebber/deepc-challenge-tcp:v0.0.4 .

docker-push: docker-image
	docker push andrewvwebber/deepc-challenge-tcp:v0.0.4

helm-install:
	helm install -n deepc-challenge ./deployment/kubernetes/helm/deepc-challenge/

helm-upgrade:
	helm upgrade deepc-challenge ./deployment/kubernetes/helm/deepc-challenge/
